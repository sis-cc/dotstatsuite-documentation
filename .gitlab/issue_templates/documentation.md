<!---

Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues.

--->

### Description
[Provide a brief description of the documentation change or addition needed.]

### Affected Documentation
- [ ] User Guide
- [ ] API Reference
- [ ] Developer Documentation
- [ ] README
- [ ] Other: [Specify]

### Proposed Changes
[Outline the specific changes or additions to be made.]

### Reason for Change
[Explain why this documentation change is necessary.]

### Organisation using the .Stat Suite
[What is the organisation using the .Stat Suite?]

<!---

/label ~t::documentation ~product-management ~s::review

--->
