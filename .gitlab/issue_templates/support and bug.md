<!---

Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues. You may also find an answer in our online documentation: https://sis-cc.gitlab.io/dotstatsuite-documentation/.

--->

### Summary of the issue
[Summarize the issue you encounter.]

### Steps to reproduce and live example
- [Describe a step-by-step guide to reproduce the issue (how it occurs).]
- [Attach all relevant logs, screenshots, test files (e.g., data.csv, structure.xml) that are required for the undertanding and the analysis.]
- [Link, whenever possible, example(s) of the actual behaviour (that also indicates the environment).]

### Expected behaviour
[Describe how it would behave, according to your own understanding.]

### .Stat Suite version
[Provide the .Stat Suite version (release name, components version) for which the issue occurs.]

### Organisation using the .Stat Suite
[What is the organisation using the .Stat Suite?]

<!---

/label ~t::support ~product-management ~s::review

--->
