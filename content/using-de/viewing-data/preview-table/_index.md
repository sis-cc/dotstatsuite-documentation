---
title: "Preview table"
subtitle: 
comments: false
weight: 2500
keywords: [
  'Role of preview-tables', '#role-of-preview-tables',
  'Preview-table components', '#preview-table-components',
  'Layout rules', '#layout-rules',
  'Display of observations values', '#display-of-observations-values',
  'Display of qualitative observations', '#display-of-qualitative-observations',
  'Display of additional information', '#display-of-additional-information',
  'Display of hierarchical dimensions', '#display-of-hierarchical-dimensions',
  'Display of advanced hierarchies', '#display-of-advanced-hierarchies',
  'Management of empty columns', '#management-of-empty-columns',
  'Management of empty rows', '#management-of-empty-rows',
  'Management of empty row sections', '#management-of-empty-row-sections',
  'Horizontal scroll', '#horizontal-scroll',
  'Vertical scroll', '#vertical-scroll',
  'Scroll back arrows', '#scroll-back-arrows', 
  'Horizontal merged column header cells', '#horizontal-merged-column-header-cells',
  'Highlighted cells, rows and columns', '#highlighted-cells-rows-and-columns',
  'Extreme cases of layout', '#extreme-cases-of-layout',
  'Links to underlying microdata', '#links-to-underlying-microdata',
]
---

#### Table of Content
- [Role of preview-tables](#role-of-preview-tables)
- [Preview-table components](#preview-table-components)
- [Layout rules](#layout-rules)
- [Display of observations values](#display-of-observations-values)
- [Display of qualitative observations](#display-of-qualitative-observations)
- [Display of additional information](#display-of-additional-information)
- [Display of hierarchical dimensions](#display-of-hierarchical-dimensions)
- [Display of advanced hierarchies](#display-of-advanced-hierarchies)
- [Management of empty columns](#management-of-empty-columns)
- [Management of empty rows](#management-of-empty-rows)
- [Management of empty row sections](#management-of-empty-row-sections)
- [Horizontal scroll](#horizontal-scroll)
- [Vertical scroll](#vertical-scroll)
- [Scroll back arrows](#scroll-back-arrows)
- [Horizontal merged column header cells](#horizontal-merged-column-header-cells)
- [Highlighted cells, rows and columns](#highlighted-cells-rows-and-columns)
- [Extreme cases of layout](#extreme-cases-of-layout)
- [Links to underlying microdata](#links-to-underlying-microdata)

---

### Role of preview-tables
The preview-tables are not meant to be publication-ready statistical tables. They are only meant to provide a quick mean for users to validate that they have found the data that they were looking for.  
Therefore, larger online table displays (and the related Excel table downloads) are not supported in order to assure high performance. However, larger amounts of selected data can still be downloaded using the native SDMX formats (SDMX-CSV, SDMX-ML, SDMX-JSON).  
If publication-ready statistical tables are required, then they should be implemented using the feature to download external resources (e.g. ready-made data extractions or tables) based on the "EXT_RESOURCE" dataflow annotation. 

The preview-table is constructed out of the data resulting from the SDMX query that corresponds to the curent data selection in the filters. In order to prevent too large tables to be built and the web browser to freeze due to memory overload, the SDMX query includes a predefined, configurable range limit, e.g. 2500 observations. If the range limitation needed to be used because the unlimited result would have exceeded the limit, then a warning message is displayed in the second table sub-title. For more details see [this section](/dotstatsuite-documentation/using-de/viewing-data/preview-table/incomplete-data/). 

For performance reasons, the maximum number of table rows, columns and cells are also limited through configuration.

Because the SDMX API does not allow yet for pro-active pagination, only the first page of data can currently be previewed.

The following specific preview-table actions are proposed:  
- [download](/dotstatsuite-documentation/using-de/viewing-data/toolbar/#table-in-excel) as-is preview-table as Excel file (xslx)

---

### Preview-table components
The preview-table has a **header** (title, first subtitle, second subtitle (unit)), a **footer** (source, logo, copyright) that are common with the charts - for more details see [here](/dotstatsuite-documentation/using-de/viewing-data/common-header-and-footer/) - and a **table grid**. This table grid is a three-dimensional pivot table with the following axes:  
- **Column**
- **Row section** (groupings of rows with the same partial dimension selection)
- **Row**

The classical feature of the pivot table 'filter' axis (like in Excel pivot tables) is assured by the visualisation page [filters](/dotstatsuite-documentation/using-de/viewing-data/filters/). Therefore, the table grid does not have its own additional 'filter' selector.

The dimensions on the **column axis** are shown with **1 header row per dimension**, vertically indented for hierarchical dimensions (see more details in [this section](#display-of-hierarchical-dimensions)).

The dimensions on the **row section axis** are shown flattened, one per line. For hierarchical dimensions, each child value is shown with its auto-concatenated parent(s) in a [breadcrumb-like feature](#display-of-hierarchical-dimensions). Row section rows always span horizontally over the complete table width. They help and enhance the table readability by avoiding (too many) repeated or imbricated row header cells.  

The dimensions on the **row axis** are shown with **1 header column per dimension**, horizontally indented for hierarchical dimensions (see [this section](#display-of-hierarchical-dimensions) for more details). There is no vertical cell merging when vertically neighboured row header cells have the same value. Whenever possible, there must always at least one dimension be on the row axis. However, it is encouraged to use more than 1 row dimension only in cases when statistically absolutely necessary.

The distribution of the different data dimensions over the pivot table axes is called **layout**. See below how the layout is determined. 

---

### Layout rules

Either initially or after any user interaction, **each table is always reconstructed** based on the following rules (in this order):

1) **In-built default** business rules
   - the TIME_PERIOD dimension is shown in the column axis
   - the REF_AREA dimension is shown in the row axis
   - all other dimensions are shown in the row section axis 
2) which are overwritten by the default selections and layout defined in **annotations**
   - [Default layout](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-layout/)
   - [Default filter selections](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-selection/)
3) which are overwritten by selections and layout submitted through **URL parameters**
4) which are overwritten by the **last interactive user changes** in selections and layout done in the current page
   - [Customise table layout](/dotstatsuite-documentation/using-de/viewing-data/preview-table/customise-feature/).

❗On top of this underlying current state of layouting, the following business rules alter the rendering **in a non-persisting way** based on the component combination logic and on unicity of the available values, and are thus to be re-applied for each table update:

5) All the non-hidden dimensions configured to be part of a **combined virtual components**, despite their current layout attributions, are, when possible, merged together with the attributes configured to be part of the combined virtual components. These virtual components are displayed separately on automatically determined axes based on the rules defined here:  
   - [Special display of 'combined concepts' in the preview table](/dotstatsuite-documentation/using-de/viewing-data/preview-table/combined-concepts/).
6) All dimensions not included in combined virtual components and with exactly one available item are excluded from the table axes and displayed instead in the first table **subtitle**. However, if those single-valued dimensions have a  "NOT_DISPLAYED" annotation, then they are hidden:  
   - [Hide information of a data view](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/not-displayed/)  

   ➡️ This is to minimise the required table size and complexity, thus eases the interpreting of table cells.
7) If there is no Row dimension yet, then the first available dimension not yet attributed to any axis is moved to the **row axis**. If there is no such one, then the last dimension from either the row section axis or the columns axis (in this order), is moved to the Row axis.  
   ➡️ This is to assure that there is always *at least one dimension in the row axis*, unless all dimensions are single-valued, since vertically layed-out tables are easier to read on screens than horizontally ones. This is also required to avoid the *strange* layout case when there are dimensions on the row section axis but none on the row axis.  
8) If there is no Column dimension yet, then the first available dimension not yet attributed to any axis is moved to the **column axis**.  
   ➡️ This is to more harmoniously distribute table cells over columns and rows.  
9) All other dimensions not yet attributed to any axis are moved to the **row section axis**.  
   ➡️ This is to minimise the required table size and complexity, thus eases the interpreting of table cells, especially if the row section dimensions don't have hierarchies.  

Furthermore, the table layout also takes the following customisations into account:

* [Truncated data responses](/dotstatsuite-documentation/using-de/viewing-data/preview-table/incomplete-data/)
* [Increased table size](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/increased-table-size)
* [Implicit and explicit orders](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/implicit-explicit-order/)
* [Notes](/dotstatsuite-documentation/using-de/viewing-data/preview-table/footnotes/)
* [Extra cells in the table for attribute values](/dotstatsuite-documentation/using-de/viewing-data/preview-table/attributes-cells/)
* [Custom data alignment](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/custom-data-alignment/)
* [Reversed time period order](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/reversed-time-period/)
* [Non-calendar reporting periods](/dotstatsuite-documentation/using-de/viewing-data/preview-table/non-calendar-year-report/)
* [Irregular time periods](/dotstatsuite-documentation/using-de/viewing-data/preview-table/irregular-time-periods/)

---

### Display of observations values
**Observation values in numerical formats** (real, decimal, ...) are displayed as 'Numbers' with thousand separators (e.g. " " (non-breaking space), "." or "'") and a decimal separator (e.g. "." or ",") according to the predefined, configurable locale, and with the fixed number of decimals after the decimal separator as specified in the relevant "DECIMALS" attribute.  

**Example:**   
3 512 032.00

Other **non-numeric** and **coded measure values formats** are supported and detailed (with example) [here](/dotstatsuite-documentation/using-api/core-data-model/#data-type-definitions).

---

### Display of qualitative observations
> *Version history:*  
> Wrapped textual values since [December 20, 2023 Release .Stat Suite JS yay](/dotstatsuite-documentation/changelog/#december-20-2023)

Observations values in textual formats can be displayed in the table view when the *SDMX* representation of the measure is of textual type, e.g. 'String' (see the list of supported representation types [here](/dotstatsuite-documentation/using-api/core-data-model/#data-type-definitions)).  
When the textual values are too long, then the text is wrapped so that the table length fits to the length of the web-browser page.

![Table with qualitative observations](/dotstatsuite-documentation/images/de-table-qualitative.png)

---

### Display of additional information
> *Version history:*  
> *footnotes* replaced by *notes* in [March 4, 2022 Release .Stat Suite JS 13.0.0](/dotstatsuite-documentation/changelog/#march-4-2022)  
> Enhanced with [February 21, 2022 Release .Stat Suite JS 12.1.0](/dotstatsuite-documentation/changelog/#february-21-2022)

Data attributes and referential metadata can be displayed for preview tables and charts in different ways: through flags, notes, or icons with a link to information displayed in a side panel. 

**Flags**  
Directly displays the IDs of related coded attribute values (with maximal 4 characters) next to the observation value. The localised names of the attribute values are displayed on mouse-over in a tooltip bubble.  
Attributes are displayed through flags only if the code ID of the attribute value is not longer than 4 characters and only when requested by configuration:
- per dataflow in its annotation, see [LAYOUT_FLAG entry here](/dotstatsuite-documentation/using-de/sdmx-annotations/#user-managed-annotations), otherwise
- per Data Explorer scope in its settings, see [here](/dotstatsuite-documentation/configurations/de-configuration/#coded-attributes-returned-as-flags)

**Notes**  
Displays a star icon `*` at the appropriate places as documented [here](/dotstatsuite-documentation/using-de/viewing-data/preview-table/footnotes/). The related attribute values are displayed on mouse-over in a tooltip bubble.  
Attributes are displayed through notes only if they should have been displayed through flags but could not because the code ID of the attribute value is longer than 4 characters or when requested by configuration:
- per dataflow in its annotation, see [LAYOUT_NOTE entry here](/dotstatsuite-documentation/using-de/sdmx-annotations/#user-managed-annotations), otherwise
- per Data Explorer scope in its settings, see [here](/dotstatsuite-documentation/configurations/de-configuration/#coded-and-uncoded-attributes-returned-as-notes)

**Information icon and side panel**  
Displays an information icon `(i)` at the appropriate places and the related attribute and referential metadata values are displayed on mouse-over in an information side panel as documented [here](/dotstatsuite-documentation/using-de/viewing-data/preview-table/information-panel).  
All attributes and referential metadata are displayed with an information icon `(i)` unless:
- they are already displayed through flags (see above),
- they are already displayed through notes (see above) or
- they are defined to not to be displayed through the related dataflow annotation, see [NOT_DISPLAYED entry here](/dotstatsuite-documentation/using-de/sdmx-annotations/#user-managed-annotations).  


*Configuration examples:* 

Configuration in dataflow annotations:
```json
"annotations": [
    {
        "title": "OBS_STATUS,CONF_STATUS",
        "type": "LAYOUT_FLAG"
    },
    {
        "title": "NOTE1,NOTE2",
        "type": "LAYOUT_NOTE"
    },
    {
        "title": "DECIMALS",
        "type": "NOT_DISPLAYED"
    }
]
```

Configuration in Data Explorer configuration settings:  
```json
"sdmx": {
    "attributes": {
      "flags": ["OBS_STATUS", "CONF_STATUS"],
      "notes": ["NOTE"]
    },
}
```

#### The following display rules are applied with ascending prevalence for any attribute
(7) The DE configuration `notes` defines which attributes are displayed as `note`.  
(6) The DE configuration `flags` defines which attributes are displayed as `flag`.   
(5) The DF annotation `LAYOUT_NOTE` fully overwrites the list in DE configuration `notes` and instead defines which attributes are displayed as `note`.  
(4) The DF annotation `LAYOUT_FLAG` fully overwrites the list in DE configuration `flags` and instead defines which attributes are displayed as `flag`.  
(3) The DF annotations `UNIT_MEASURE_CONCEPTS`, `DRILLDOWN_CONCEPTS` and `DRILLDOWN` define which attributes are displayed differently due to other (already existing, being implemented or planned) features, even if they have been listed in any of the previously mentioned settings.  
(2) The DF annotation `NOT_DISPLAYED` defines which attributes are **not displayed** at all even if they have been listed in any of the previously mentioned setting.  
(1) All attributes not defined by the previously listed settings are indicated by an information icon `(i)` and shown in the `information` side panel. 

**Rule's exception for Flags**  
If the value ID of an attribute that is defined as flag is longer than 4 characters, then this value will not be displayed as a flag but as a note.

---

### Display of hierarchical dimensions
>*Version history:*
> Display empty parents as empty rows and columns since [December 20, 2023 Release .Stat Suite JS yay](/dotstatsuite-documentation/changelog/#december-20-2023)  
> Single-item selection displayed in sub-header since [September 20, 2023 Release .Stat Suite JS 'xray'](/dotstatsuite-documentation/changelog/#september-20-2023)  
> Indentation on column headers since [December 5, 2022 Release .Stat Suite JS 'spin'](/dotstatsuite-documentation/changelog/#december-5-2022)  
> Extended to all dimensions + dots with [December 14, 2021 Release .Stat Suite JS 11.0.0](/dotstatsuite-documentation/changelog/#december-14-2021)  
> Introduced in [October 8, 2019 Release .Stat Suite JS 2.0.0](/dotstatsuite-documentation/changelog/#october-8-2019) 

**On the row axis:**
- All **dimensions with hierarchies** on the row axis are **indented** (one middle dot `"·"` and figure space `" "` per level of indentation) according to the level of hierarchy.
- In case of long titles (two lines or more), then all lines are indented (all lines would start at the same place as the first one).
**Hierarchies on row axis with empty parents:**
- Selected parents without observations are displayed in the table as a separate empty rows. Their value cells are empty (instead of .. which is used for observations without observation values).

![Table with hierarchical dimensions](/dotstatsuite-documentation/images/de-table-empty-parent-rows.png)
  
**On the column axis:**
- All column header cells are top-aligned including the dimension label cell
- All **dimensions with hierarchies** on the column axis are **vertically indented** (middle dot `"·"` with a line break per per level of indentation)

![Table with hierarchical dimensions](/dotstatsuite-documentation/images/DE_table_hierarchy_column.PNG)

**Hierarchies on column axis with empty parents:**
- Selected parents without observations are displayed in the table as a separate empty columns. Their value cells are empty (instead of .. which is used for observations without observation values).
- Each parent with empty observations is still vertically indented. 

![Table with hierarchical dimensions](/dotstatsuite-documentation/images/de-table-empty-parent-columns.png)

**On the row section axis:**
- In case when a child with an observation value is displayed in the table along with its selected parent, then the parent name is displayed as prefix of the child followed by ">" 'greater than'.
- In case of multiple parents' levels, then only the direct parent of a child is displayed as a breadcrumb (as prefix).

**Hierarchies on row section axis with empty parents:**
- When a hierarchical dimension is displayed on the row section axis, and a parent of a selected child is also selected and has no data, then the display is the same as when the parent has data.

**Additional rules for empty parents:**
- `NOT_DISPLAYED` (see [Hide information of a data view](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/not-displayed/#hide-information-of-a-data-view)) continues to be applied only to single fixed dimensions or with the (`CODE1+CODE2`) syntax. Those values remain hidden.
- If a parent (of a selected child) is marked as `NOT_DISPLAYED` but is selected, then both parent and child are displayed.
- However, independently from the selections, `NOT_DISPLAYED` continues to be always applied within combined dimensions (see [Combined concepts](/dotstatsuite-documentation/using-de/viewing-data/preview-table/combined-concepts/)).
- In general for Combined dimensions, whenever an empty hierarchical parent is added, then it is added only once and all following dimensions in the combination are omitted from that combination.
- Rows of combinations at root level are omitted if they have no data. The (grand-)parent(s) row without data are also omitted if there are no child rows to be displayed.

See more details about [Selectable empty parent in hierarchical filters](/dotstatsuite-documentation/using-de/viewing-data/filters/#hierarchical-content/selectable-empty-parent-in-hierarchical-filters) and [Automated selected parents](/dotstatsuite-documentation/using-de/viewing-data/filters/#hierarchical-content/automated-selected-parents)

---

### Display of advanced hierarchies
See https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/filters/advanced-hierarchies/

---

### Management of empty columns
Whenever a column is empty, it is automatically excluded from the table, **except** for selected empty parent(s) of a hierarchical dimension (see [this section](#display-of-hierarchical-dimensions) above).

---

### Management of empty rows
Whenever a row is empty, it is automatically excluded from the table, **except** for selected empty parent(s) of a hierarchical dimension (see [this section](#display-of-hierarchical-dimensions) above).

---

### Management of empty row sections
Whenever a complete row section is empty, it is automatically excluded from the table.  

---

### Horizontal scroll
>*Version history:*  
> Optimized in order to simplify the general page behavior with [August 3, 2022 Release .Stat Suite JS 'quark'](/dotstatsuite-documentation/changelog/#august-3-2022)  
> Enhanced with [March 4, 2022 Release .Stat Suite JS 13.0.0](/dotstatsuite-documentation/changelog/#march-4-2022)  
> Introduced in [January 13, 2022 Release .Stat Suite JS 12.0.0](/dotstatsuite-documentation/changelog/#january-13-2022)

If the table cannot horizontally fit on the available screen, then the screen allows horizontal scrolling.  
When scrolling horizontally, only the filter area and the table move. The page header, page footer and the toolbar menu positions are frozen. The table header (title and subtitle), row header column(s) and the row section header(s) move only until reaching the edge of the browser window (they are defined as "css-sticky"), unless there is not enough space for the remaining data columns, in which case it/they keep(s) individually scrolling as much as possible. The table footer position is frozen.

Freezing table parts is not available when [Accessibility support](/dotstatsuite-documentation/using-de/general-layout/#web-content-accessibility-support) is enabled.

![DE table horizontal scrolling](/dotstatsuite-documentation/images/de-table-horizontal-scroll.GIF)

---

### Vertical scroll
If the table cannot vertically fit on the available screen, then the screen allows vertical scrolling (but infinite dynamic scroll is not implemented).  
When scrolling vertically, the whole page content scrolls but the column header row(s) and the row section header(s) move only until reaching the edge of the browser window (as they are defined as "css-sticky") and the row section header row(s) stop(s) scrolling before covering the column header row(s).

Freezing table parts is not available when [Accessibility support](/dotstatsuite-documentation/using-de/general-layout/#web-content-accessibility-support) is enabled.

![DE table vertical scrolling](/dotstatsuite-documentation/images/de-table-vertical-scroll.GIF)

---

### Scroll back arrows
> Introduced in [March 4, 2022 Release .Stat Suite JS 13.0.0](/dotstatsuite-documentation/changelog/#march-4-2022)

A "fast-forward left" button and a "fast-forward top" button appear when the user has started scrolling whther horizontally or vertically. Using one of these buttons, the user can quickly go back to the initial horizontal or vertical scroll position.  
Once the user is back to the initial horizontal scroll position, then the "fast-forward left" button disappears.  
Once the user is back to the initial vertical scroll position, then the "fast-forward top" button disappears.

![DE table scroll and arrows](/dotstatsuite-documentation/images/de-table-scroll-arrows.png)

---

### Horizontal merged column header cells
> Released in [May 19, 2021 Release .Stat Suite JS 8.0.0](/dotstatsuite-documentation/changelog/#may-19-2021)

When 2 or more dimensions are positioned on the Column axis, and if 2 or more horizontally neighboured cells have the same value (dimension item), then **these cells are merged**.  
Merging is not applied to cells that have different cells in rows above them, thus never to cells in the last (lowest) column header row.  
The merged column header labels are centered by default, but whenever the table width is larger than the width of the table display space, then it sufficiently moves within the cell to the left or right in order to stay visible even **when scrolling horizontally**.

![DE table merged header cells](/dotstatsuite-documentation/images/de-table-merged-cells.PNG)

**Note** that this feature doesn't apply to the [html WCAG table](/dotstatsuite-documentation/using-de/general-layout/#web-content-accessibility-support).

---

### Highlighted cells, rows and columns
> Released in [April 1, 2021 Release .Stat Suite JS 7.2.0](/dotstatsuite-documentation/changelog/#april-1-2021)

A table cell is highlighted with a blue border when mouse-hovered or selected.    
Cells can be selected and deselected through double-click.  
When a cell is selected, then also all cells in the same row and same column (except column header cells) are background-coloured with a specific highlight colour (Neutral lighten-3 #FEF4E6). Row sections and column headers are not impacted.  
The highlight remains when switching the representation from table to chart and table again, but is removed when either the cell selection, the table layout or the data selection change.

![DE table view highlight](/dotstatsuite-documentation/images/de-preview-table-highlight.png)

---

### Extreme cases of layout
When all dimensions have **single(-fixed) values**, in which case there are no Column, Row section and Row dimensions and thus only one single value is to be displayed, then the special attribute row and column are hidden and flags/footnotes is shown directly in the single cell:

![DE_table_scrolling](/dotstatsuite-documentation/images/DE_table_single_value.png)

In case there is **no Column** dimension, then the special attribute column is hidden and flags/footnotes are shown directly in the cell(s).

![DE_table_scrolling](/dotstatsuite-documentation/images/DE_table_responsiveness.png)

Since there must always be at least one dimension in the Row axis (unless all dimensions have single(-fixed) values - see above case) the following table layout is **not possible**:

![DE_table_scrolling](/dotstatsuite-documentation/images/DE_table_without_row_dims.png)

---

### Links to underlying microdata
For any DSD and dataflow fulfilling the conditions for microdata, the Data Explorer will add a hyperlink to each table value cell that allows (through clicking the link) opening the microdata viewer tab which displays those microdata that are linked to that aggregated data value.

![de-microdata-1](/dotstatsuite-documentation/images/de-microdata-1.png)

For more information, see [here](/dotstatsuite-documentation/using-de/viewing-data/microdata-preview-table/).
