---
title: "Disabled data availability"
subtitle: 
comments: false
weight: 2750

---

>*Version history:*  
> Introduced with [September 23, 2024 Release .Stat Suite JS baryon](/dotstatsuite-documentation/changelog/#september-23-2024)  

This feature allows Data Owners to **disable specific (SDMX API) data availability** queries for dataflows with large content, preventing potentially heavy queries that could overload the server and reduce its performance.

To do so, a new *SDMX* annotation of type **`DISABLE_AVAILABILITY_REQUESTS`** is introduced and attached to the dataflow definition at the structure level.

Example:

```json
"annotations": [{
    "type": "DISABLE_AVAILABILITY_REQUESTS"
}]
```

When the `DISABLE_AVAILABILITY_REQUESTS` annotation is set for a dataflow:
1. The Data Explorer does not make any availability requests used for non-time/frequency filters that are typically fired together with data queries;
1. The visualisation filter items are not greyed out for data unavailability;
1. The Data Explorer continues to make availability requests only for time/frequency filters.

In addition, a warning message is displayed below the sub-title (black text with an orange background) to inform end-users when data availability is disabled: "Due to the large size of this dataset, we are not able to indicate unavailability of data for certain filter selections."

![DE disabled data availability](/dotstatsuite-documentation/images/de-disabled-data-availability.png)
