---
title: "Extra cells in the table for attribute values"
subtitle: 
comments: false
weight: 2670
keywords: [
  'Table layout option', '#table-layout-option',
]

---

>*Version history:*  
> Introduced with [September 18, 2024 Release .Stat Suite JS baryon](/dotstatsuite-documentation/changelog/#september-18-2024)  

A table layout option allows displaying (non-hidden) **observation-level attribute values in separate table cells** next to the corresponding observation value cell (instead of the default flags or note inside the observation value cell).  
Only if there are any non-hidden observation-level attributes that are not already included in the [Combined unit of measure](/dotstatsuite-documentation/using-de/viewing-data/preview-table/combined-concepts/), then a checkbox is shown in the table "Layout" configuration titled "Show values and related characteristics in separate table cells".

![Table layout option for extra cells](/dotstatsuite-documentation/images/de-extra-cells1.png)

When the box is checked, then add a **"fake" dimension** labeled "Values and related characteristics" (and with ID "OBS_ATTRIBUTES") is added to the layout table, positioned by default in the column axis. The user can move this "fake" dimension around the table axis like any other dimension.

![Table layout option for extra cells](/dotstatsuite-documentation/images/de-extra-cells2.png)

Clicking on "Apply layout" will update the table view with the new layout including the new "Values and related characteristics" dimension.  
The table view with the option checked is composed of
- the extra "Values and related characteristics" dimension
- "Value" and non-hidden observation-level attribute names as dimension values (except those included in the Combined unit of measure)
- Observation values and related non-hidden observation-level attribute values as cell values (each value has its own cell)

![Table layout option for extra cells](/dotstatsuite-documentation/images/de-extra-cells3.png)


This layout with extra cells is preserved when the user downloads the **table in Excel format**.

Also, users can define the "fake" dimension for observation-level attributes as **default display** for a dataflow or a DSD, by adding the dimension ID `"OBS_ATTRIBUTES"` to one of the related annotations "LAYOUT_ROW", "LAYOUT_COLUMN" or "LAYOUT_ROW_SECTION", e.g.

```json
       "annotations": [{
               "type": "LAYOUT_COLUMN",
               "title": "DIM1,OBS_ATTRIBUTES"
       }]
```
