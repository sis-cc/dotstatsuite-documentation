---
title: "Applied filters panel and data points"
subtitle: 
comments: false
weight: 2250
keywords: [
  'Header', '#header',
  'Applied filters panel', '#applied-filters-panel',
]
---

#### Table of Content
- [Header](#header)
- [Applied filters panel](#applied-filters-panel)

---

> *Version history:*  
> The DE has been updated to improve comprehension and consistency of the "Applied filters" section. It is renamed "x data points selected in this dataset with:" since [January 20, 2025 Release .Stat Suite JS causality](/dotstatsuite-documentation/changelog/#january-20-2025)  
> Data points feature released with [December 20, 2023 Release .Stat Suite JS yay](/dotstatsuite-documentation/changelog/#december-20-2023)

### Header
The applied filters area display an informative message about the currently selected data points named **"[X] data points selected in this dataset with:"** where [X] represents the nuber of data points for the current selection.  
The data points number is obtained from the available constraint obtained from the current user selection. The number of data points is dynamically calculated each time the user modify the current selection.  
If the number of data points is not yet known (before the available constraint query is completed), then it will temporarily display only "Data selected in this dataset with:".

![applied filters](/dotstatsuite-documentation/images/de-view-applied-filters-collapsed.png)

**Expand/Collapse**  
The entire header line is clickable to expand or collapse the applied filters area, in addition to the left-sided arrow.

![expanded content](/dotstatsuite-documentation/images/de-view-applied-filters-expanded.png)

**Clear filters**  
Users are provided with easy options to unselect individual dimension items or whole dimension selections:
* one single item by clicking on the `x` next to the item label, or
* all items for a given dimension by clicking on the `x` next to the dimension label, or finally
* all selections by clicking on `Clear all`.

### Applied filters list
The applied filters list displays all currently selected items per dimension. Each selected item is shown on a separate line, grouped by dimension. The format is as follows:

- Dimension name: Selected item 1, Selected item 2, ...

For example:
- Time: 2022, 2023
- Country: France, Germany, Italy

If a dimension has hierarchical items, child items are displayed with their full hierarchy path:
- Geography: Europe > France, Europe > Germany, North America > United States

![hierarchical content](/dotstatsuite-documentation/images/de-view-applied-filters-hierarchy.png)

When more than 15 items are selected for the same dimension, the display of the individual selected items is replaced by a number being the number of selected items for the specific facet.

![too many items](/dotstatsuite-documentation/images/de-view-applied-filters-too-many.png)

The applied filters list is dynamically updated as the user modifies their selection. When viewing the microdata table, the 'Applied filters' panel is not displayed.


