---
title: "Flight planner"
subtitle: 
comments: false
weight: 13
keywords: [
  'Flight planner', '#flight-planner',
  'Milestones board', '#milestones-board',
]

---


### Flight planner
**This flight planner allows travellers [*implementers*] getting a better understanding of the foreseen flight stops [*availability of features*] and their approximate timing according to the current fuel level [*resource capacity*] and Community priorities, and so to plan their own flight boarding [*deployment strategy*] as best as possible.**

![Flight Planner](/dotstatsuite-documentation/images/flight_planner.png)  

Information about the .Stat Suite releases can be found in the **[changelog](/dotstatsuite-documentation/changelog/)** page.

Our **scheduled backlog** represents our short-term delivery strategy for the upcoming evolutions of the .Stat Suite products. In this [board view](https://gitlab.com/groups/sis-cc/.stat-suite/-/boards/7061159), each milestone will be an attempt to produce a .NET/SQL or a JavaScript release. The tickets in each milestone can be subject to change of order or delay. You can view the ongoing work (milestone(s) in progress) in the **[Kanban boards](https://gitlab.com/groups/sis-cc/-/boards/834539)**.

The full list of recorded feature requests and feature enhancements can be found in the prioritized **[wish list](https://gitlab.com/groups/sis-cc/-/issues?label_name%5B%5D=wish+list&scope=all&sort=weight_desc&state=opened&utf8=%E2%9C%93)**.


### Milestones board
The **Milestones board** view displays the in-progress milestones for each .NET and JavaScripts releases: https://gitlab.com/groups/sis-cc/.stat-suite/-/boards/7061159
