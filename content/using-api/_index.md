---
title: "Using .Stat Suite APIs"
subtitle: 
comments: false
weight: 4000

---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/using-dotstat-suite/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

![Core logo](/dotstatsuite-documentation/images/core_logo.png)

* [Main APIs features](/dotstatsuite-documentation/using-api/api-main-features/)
* [.Stat Suite Core data model](/dotstatsuite-documentation/using-api/core-data-model/)
* [Data features](/dotstatsuite-documentation/using-api/data/)
* [Referential metadata features](/dotstatsuite-documentation/using-api/ref-metadata/)
* [Typical use cases](/dotstatsuite-documentation/using-api/typical-use-cases/)
* [.Stat SDMX RESTful Web Service Cheat Sheet](/dotstatsuite-documentation/using-api/restful/)
* [Data synchronisation](/dotstatsuite-documentation/using-api/data-synchronisation/)
* [Embargo management (Point in Time)](/dotstatsuite-documentation/using-api/embargo-management/)
* [Confidentiality and embargo data](/dotstatsuite-documentation/using-api/confidential-data/)
* [Email notifications for data management](/dotstatsuite-documentation/using-api/message-through-mail/)
* [Programmatic authentication](/dotstatsuite-documentation/using-api/programmatic-auth/)
* [Permission management](/dotstatsuite-documentation/using-api/permission-management/)
* [Caching system](/dotstatsuite-documentation/using-api/caching-system/)
