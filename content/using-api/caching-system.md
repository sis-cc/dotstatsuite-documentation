---
title: "Caching system"
subtitle: ""
comments: false
weight: 15000
keywords: [
  '', '',
]
---

> Introduced with [September 27, 2024 Release .Stat Suite .NET "gingerbread"](/dotstatsuite-documentation/changelog/#september-27-2024)  

---

This feature introduces a **response caching mechanism for the NSI Web Service** to improve performance, particularly for public dissemination instances. The caching configuration allows for more efficient handling of dynamic responses, it reduces the load on the server, and it improves response times for frequently requested data.

This first implementation has the following current limitations:
- Caching applies only for non-authenticated users
- HTTP 206 (Partial Content) responses are not cached by default

Read more details about how to configure the caching mechanism and its multiple supported parameters [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws#response-caching-configurarion). You can also consult [this page](https://learn.microsoft.com/en-us/aspnet/core/performance/caching/middleware?view=aspnetcore-8.0#conditions-for-caching) summarizing the main conditions and parameters allowing caching to work in our context.
