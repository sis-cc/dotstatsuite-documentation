---
title: "Report an issue"
subtitle: 
comments: false
weight: 103
---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/contribute/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

### New issue
If you want to report an issue for any of the .Stat Suite applications or services, then you need to **open a new issue** in [Gitlab sis-cc/.stat-suite/issues](https://gitlab.com/groups/sis-cc/.stat-suite/-/issues).  
You can first try to find out if your issue (a bug, a new feature or a feature enhancement) has already been recorded.  

Any new issue needs to be created in a specific .Stat Suite project. The list being quite long, if you don't know to which specific service, tool or application to refer to, then go for the simplest, e.g. *dotstatsuite-data-explorer*, *dotstatsuite-data-lifecycle-manager* or *dotstatsuite-core-common*.  

we are using the GitLab issue Templates in our DevSecOps process. This powerful feature helps us to standardise our issue creation, to improve communication, and to increase efficiency. To get started with the issue templates:
1. We have created templates for common issue types, e.g., "support and bug", "enhancement", "documentation".
2. You will find these templates when creating new issues in any of our GitLab projects.
3. Simply select the appropriate template and fill in the required information.

If you want to fix a bug yourself, then please refer to the [Development guidelines](https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/development-guidelines/).  
