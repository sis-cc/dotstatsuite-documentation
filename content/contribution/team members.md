---
title: "Team members"
subtitle: 
comments: false
weight: 110
---

#### .Stat Suite developers
- Aleksei Arhipov [@Aleksei-oecd](https://gitlab.com/aleksei-oecd)
- Amal Lahmar [@amallahmar](https://gitlab.com/amallahmar)
- Balazs Szilagyi [@balazs82](https://gitlab.com/balazs82)
- Dimitri Roncoli [@RedPDRoncoli](https://gitlab.com/RedPDRoncoli)
- Nicolas Briemant [@nicolas-briemant](https://gitlab.com/nicolas-briemant)
- Pedro Carranza [@pedroacarranza](https://gitlab.com/pedroacarranza)
- Zsolt Lenart [@ZsoltLenart](https://gitlab.com/ZsoltLenart)

#### .Stat Suite product owners
- Jean-Baptiste Nonin [@j3an-baptiste](https://gitlab.com/j3an-baptiste)
- Jens Dossé [@dosse](https://gitlab.com/dosse)
- Laura Belli [@LauraBelli](https://gitlab.com/LauraBelli)
- Sandrine Phelipot-Souflis [@SSandrine](https://gitlab.com/SSandrine)

#### .Stat Suite community manager
- Jonathan Challener [@ChallenerSJ](https://gitlab.com/ChallenerSJ)
