---
title: "Matomo"
subtitle: 
comments: false
weight: 77
keywords: []

---

**[Matomo](https://matomo.org/)** is a web analytics solution platform, similarly to Google Analytics solutions, that can also be set up as a third-party tool for tracking Data Explorer usages. See [how to configure](/dotstatsuite-documentation/configurations/de-configuration/#third-party-tools-integration) a Matomo instance plugged to a Data Explorer instance. 

Note that Matomo can also be used to import existing Google Analytics historical data using the same tracking events. See more details [here](https://matomo.org/guide/installation-maintenance/import-google-analytics/).

![Matomo](/dotstatsuite-documentation/images/matomo.png)
