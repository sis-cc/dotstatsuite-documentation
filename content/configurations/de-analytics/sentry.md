---
title: "Sentry"
subtitle: 
comments: false
weight: 78
keywords: []

---

> Released in [January 20, 2025 Release .Stat Suite JS causality](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#january-20-2025)  

Sentry is used to gather data from a live Data Explorer instance. Check [Sentry](https://sentry.io/) and the [data-explorer readme](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer#sentry) for more details.

### Condifuration
To configure Sentry for .Stat Suite Data Explorer, follow these steps:

1. Sign up for a Sentry account at https://sentry.io/

2. Create a new project in Sentry for your .Stat Suite instance.

3. Obtain the DSN (Data Source Name) for your Sentry project.

4. Add the Sentry configuration to your .Stat Suite environment variables:

   ```
   SENTRY_DSN=your_sentry_dsn_here
   SENTRY_ENV=production  # or another environment name
   SENTRY_RELEASE=master # free value, ie develop, master or tags
   ```

With these steps, Sentry will be configured to capture and report errors and performance data from your .Stat Suite Data Explorer instance.

### Troubleshooting and additional resources
- [Sentry Documentation](https://sentry.io/docs/)
- [Sentry Tutorial](https://sentry.io/tutorials/)
- [Sentry Community](https://sentry.io/community/)

![Sentry](/dotstatsuite-documentation/images/sentry.png)