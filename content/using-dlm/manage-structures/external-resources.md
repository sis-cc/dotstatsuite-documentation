---
title: "Add links to external resources"
subtitle: 
comments: 
weight: 510

---

> Released in [XXXXX](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#XXXXXX)

https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#additional-downloads-of-external-resources

*to be complemented* (once https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/161 is delivered)

In the meantime, you can check **[here](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#additional-downloads-of-external-resources)** the current format supported in SDMX for defining external resources to be downloaded from the DE.