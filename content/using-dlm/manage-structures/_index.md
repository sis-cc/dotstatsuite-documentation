---
title: "Manage structures"
subtitle: 
comments: false
weight: 232
---

* [Upload structures from an SDMX file (xml)](/dotstatsuite-documentation/using-dlm/manage-structures/upload-structure/)
* [Delete data structures](/dotstatsuite-documentation/using-dlm/manage-structures/delete-data-structures/)
* [List related data structures](/dotstatsuite-documentation/using-dlm/manage-structures/list-related-data-structures/)
* [Copy data structures](/dotstatsuite-documentation/using-dlm/manage-structures/copy-data-structures/)
* [Create or edit data structures](/dotstatsuite-documentation/using-dlm/manage-structures/edit-structure/)
* [Define referential metadata attributes for a data structure](/dotstatsuite-documentation/using-dlm/manage-structures/link-dsd-msd/)